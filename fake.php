<?php

require_once __DIR__.'/vendor/autoload.php';

const CSV_FILENAME = 'data/data.csv';

do {
    $amount = readline('How many datasets should be generated? ');
} while (!is_numeric($amount));

$faker = Faker\Factory::create();

// clear file
file_put_contents(CSV_FILENAME, '');

$handle = fopen(CSV_FILENAME, 'wb');
for ($i = 0; $i < (int) $amount; $i++) {
    fputcsv($handle, [
        $faker->uuid,
        $faker->dateTime()->format('d.m.Y H:i:s'),
        $faker->realText(100)
    ]);
}
fclose($handle);
