<?php
declare(strict_types = 1);

namespace Stat\Sample\Search;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CsvSearchCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('app:csv-search');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // TODO: Implement Search
        $output->writeln('Implement me!');

        return Command::SUCCESS;
    }
}
