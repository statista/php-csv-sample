# Task
Please provide a CLI command to search in any given CSV file. The provided CSV file contains thousands of data sets which sometimes are duplicates of others. Each line can be considered as a distinct data set, consisting of the id, a date field and the content field.Your command should be able to ignore duplicates and provide a possibility to search for any given word and word-part in the content field. The search should be done in a case insensitive manner in a performant way. Possible errors should be hidden from the user and be stored in a log file with the probablity to change the destination to graylog and/or email later on.

Please use the provided file `data/data.csv`.

# Docker
You can use Docker if you want. 
To create a new docker application, just run `make new`. 
After that you can setup your container with `make up` if needed.

 - With `make test` you execute your tests.
 - With `make analyse` you can check your code with phpstan
 - With `make install` you install your dependencies (which is done automatically with `make new`)
 - With `make update` you update your dependencies
 - With `make search` you execute your search
